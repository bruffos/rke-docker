FROM alpine:3.10.1
LABEL maintainer="Pontus Carlsson <pontus.carlsson@gmail.com>"

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates


ARG VERSION=0.2.6

WORKDIR /usr/local/bin
RUN wget -O rke https://github.com/rancher/rke/releases/download/v${VERSION}/rke_linux-amd64 && \
    chmod u+x rke

CMD ["rke"]
